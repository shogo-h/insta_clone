require 'rails_helper'

RSpec.describe MicropostsController, type: :controller do
  describe '#index' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      include SessionsHelper

      before do
        log_in user
        get :index
      end

      it '正常にレスポンスを返す' do
        expect(response).to be_success
      end

      it '200レスポンスを返す' do
        expect(response).to have_http_status 200
      end
    end

    context 'ログインしていない場合' do
      before do
        get :index
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#show' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:micropost) { create(:micropost, user: user) }
      include SessionsHelper

      before do
        log_in user
        get :show, params: { id: micropost.id }
      end

      it '正常にレスポンスを返す' do
        expect(response).to be_success
      end

      it '200レスポンスを返す' do
        expect(response).to have_http_status 200
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }
      let!(:micropost) { create(:micropost, user: user) }

      before do
        get :show, params: { id: micropost.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#new' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      include SessionsHelper

      before do
        log_in user
        get :new
      end

      it '正常にレスポンスを返す' do
        expect(response).to be_success
      end

      it '200レスポンスを返す' do
        expect(response).to have_http_status 200
      end
    end

    context 'ログインしていない場合' do
      before do
        get :new
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#create' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      include SessionsHelper

      before do
        log_in user
      end

      it 'Micropostを作成できる' do
        micropost_params = FactoryBot.attributes_for(:micropost, user: user)
        expect{ post :create, params: { micropost: micropost_params } }.to change{ user.microposts.count }.by(1)
      end

      it 'Micropostを作成後、ルートパスにリダイレクトする' do
        micropost_params = FactoryBot.attributes_for(:micropost)
        post :create, params: { micropost: micropost_params }
        expect(response).to redirect_to root_path
      end

      it 'Micropost作成に失敗すると、Micropost新規作成ページをレンダリングする' do
        micropost_params = FactoryBot.attributes_for(:micropost, content: '')
        post :create, params: { micropost: micropost_params }
        expect(response).to render_template(:new)
      end
    end
    context 'ログインしていない場合' do
      let!(:user) { create(:user) }

      before do
        micropost_params = FactoryBot.attributes_for(:micropost, user: user)
        post :create, params: { micropost: micropost_params }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#destroy' do
    let!(:user) { create(:user) }
    let!(:micropost) { create(:micropost, user: user) }

    context 'ログインしており、自分のMicropostを削除する場合' do
      include SessionsHelper

      before do
        log_in user
      end

      it 'Micropostを削除できる' do
        expect{ delete :destroy, params: { id: micropost.id  } }.to change{ user.microposts.count }.by(-1)
      end

      it '302レスポンスを返すこと' do
        delete :destroy, params: { id: micropost.id }
        expect(response).to have_http_status 302
      end

      it 'ルートパスにリダイレクトする' do
        delete :destroy, params: { id: micropost.id }
        expect(response).to redirect_to root_path
      end
    end

    context 'ログインしており、他人のMicropostを削除する場合' do
      let!(:other_user) { create(:user, email: 'other@example.com') }
      include SessionsHelper

      before do
        log_in other_user
        delete :destroy, params: { id: micropost.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ルートパスにリダイレクトする' do
        expect(response).to redirect_to root_path
      end
    end

    context 'ログインしていない場合' do
      before do
        delete :destroy, params: { id: micropost.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end
end
