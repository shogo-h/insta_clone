require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  describe '#create' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:micropost) { create(:micropost, user: user) }
      include SessionsHelper

      before do
        log_in user
      end

      it 'コメントを作成できる' do
        comment_params = FactoryBot.attributes_for(:comment, user_id: user, micropost_id: micropost)
        expect{ post :create, params: { comment: comment_params } }.to change{ micropost.comments.count }.by(1)
      end

      it 'コメント作成後、micropost詳細ページにリダイレクトする' do
        comment_params = FactoryBot.attributes_for(:comment, user_id: user, micropost_id: micropost)
        post :create, params: { comment: comment_params }
        expect(response).to redirect_to micropost_path(micropost.id)
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }
      let!(:micropost) { create(:micropost, user: user) }

      before do
        comment_params = FactoryBot.attributes_for(:comment, user_id: user, micropost_id: micropost)
        post :create, params: { comment: comment_params }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#destroy' do
    context 'ログインしており、自分のCommentを削除する場合' do
      let!(:user) { create(:user) }
      let!(:micropost) { create(:micropost, user: user) }
      let!(:comment) { create(:comment, user: user, micropost: micropost) }
      include SessionsHelper

      before do
        log_in user
      end

      it 'Commentを削除できる' do
        expect{ delete :destroy, params: { id: comment.id } }.to change{ micropost.comments.count }.by(-1)
      end

      it '302レスポンスを返すこと' do
        delete :destroy, params: { id: comment.id }
        expect(response).to have_http_status 302
      end

      it 'micropost詳細ページにリダイレクトする' do
        delete :destroy, params: { id: comment.id }
        expect(response).to redirect_to micropost_path(micropost.id)
      end
    end

    context 'ログインしており、他人のCommentを削除する場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user, email: 'other@example.com') }
      let!(:micropost) { create(:micropost, user: user) }
      let!(:comment) { create(:comment, user: user, micropost: micropost) }
      include SessionsHelper

      before do
        log_in other_user
      end

      it '302レスポンスを返すこと' do
        delete :destroy, params: { id: comment.id }
        expect(response).to have_http_status 302
      end

      it 'ルートパスにリダイレクトする' do
        delete :destroy, params: { id: comment.id }
        expect(response).to redirect_to root_path
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }
      let!(:micropost) { create(:micropost, user: user) }
      let!(:comment) { create(:comment, user: user, micropost: micropost) }

      it '302レスポンスを返すこと' do
        delete :destroy, params: { id: comment.id }
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        delete :destroy, params: { id: comment.id }
        expect(response).to redirect_to login_path
      end
    end
  end
end
