require 'rails_helper'

RSpec.describe SessionsController, type: :controller do
  describe '#new' do
    before do
      get :new
    end

    it '正常にレスポンスを返す' do
      expect(response).to be_success
    end

    it '200レスポンスを返す' do
      expect(response).to have_http_status 200
    end
  end

  describe '#create' do
    it 'ログインに成功後、ルートパスにリダイレクトする' do
      user = create(:user)
      post :create, params: { session: { email: user.email, password: user.password } }
      expect(response).to redirect_to root_path
    end

    it 'ログインに失敗後、ログインページをレンダリングする' do
      post :create, params: { session: { email: 'test', password: 'test' } }
      expect(response).to render_template(:new)
    end
  end

  describe '#destroy' do
    let!(:user) { create(:user) }
    include SessionsHelper

    it 'ログアウト後、ルートパスにリダイレクトする' do
      log_in user
      delete :destroy
      expect(response).to redirect_to root_path
    end
  end
end
