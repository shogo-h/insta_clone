require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  describe '#index' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      include SessionsHelper

      before do
        log_in user
        get :index
      end

      it '正常にレスポンスを返すこと' do
        expect(response).to be_success
      end

      it '200レスポンスを返すこと' do
        expect(response).to have_http_status 200
      end
    end

    context 'ログインしていない場合' do
      before do
        get :index
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#show' do
    let!(:user) { create(:user) }

    context 'ログインしている場合' do
      include SessionsHelper

      before do
        log_in user
        get :show, params: { id: user.id }
      end

      it '正常にレスポンスを返す' do
        expect(response).to be_success
      end

      it '200レスポンスを返す' do
        expect(response).to have_http_status 200
      end
    end

    context 'ログインしていない場合' do
      before do
        get :show, params: { id: user.id }
      end

      it '302レスポンスを返す' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#new' do
    before do
      get :new
    end

    it '正常にレスポンスを返す' do
      expect(response).to be_success
    end

    it '200レスポンスを返す' do
      expect(response).to have_http_status 200
    end
  end

  describe '#create' do
    it 'ユーザーを登録できる' do
      user_params = FactoryBot.attributes_for(:user)
      expect{ post :create, params: { user: user_params } }.to change{ User.count }.by(1)
    end

    it 'ユーザーを登録後、ユーザー詳細ページにリダイレクトする' do
      user_params = FactoryBot.attributes_for(:user)
      post :create, params: { user: user_params }
      user = User.last
      expect(response).to redirect_to user_path(user)
    end

    it 'ユーザー登録に失敗すると、ユーザー新規登録ページをレンダリングする' do
      user_params = FactoryBot.attributes_for(:user, email: 'userexample.com')
      post :create, params: { user: user_params }
      expect(response).to render_template(:new)
    end
  end

  describe '#edit' do
    let!(:user) { create(:user) }

    context 'ログインしている場合' do
      include SessionsHelper

      before do
        log_in user
        get :edit, params: { id: user.id }
      end

      it '正常にレスポンスを返す' do
        expect(response).to be_success
      end

      it '200レスポンスを返す' do
        expect(response).to have_http_status 200
      end
    end

    context 'ログインしていない場合' do
      before do
        get :edit, params: { id: user.id }
      end

      it '302レスポンスを返す' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#update' do
    let!(:user) { create(:user) }

    context 'ログインし、自分のユーザー情報を更新する場合' do
      include SessionsHelper

      before do
        log_in user
      end

      it 'ユーザーを更新できる' do
        user_params = FactoryBot.attributes_for(:user, name: 'noname')
        patch :update, params: { id: user.id, user: user_params }
        expect(user.reload.name).to eq 'noname'
      end

      it 'ユーザーを更新後、ユーザー詳細ページにリダイレクトする' do
        user_params = FactoryBot.attributes_for(:user, name: 'noname')
        patch :update, params: { id: user.id, user: user_params }
        expect(response).to redirect_to user_path(user)
      end

      it 'ユーザー更新に失敗すると、ユーザー編集ページをレンダリングする' do
        user_params = FactoryBot.attributes_for(:user, name: '')
        patch :update, params: { id: user.id, user: user_params }
        expect(response).to render_template(:edit)
      end
    end

    context 'ログインし、他のユーザー情報を更新する場合' do
      let!(:other_user) { create(:user, email: 'other@example.com') }
      include SessionsHelper

      before do
        log_in other_user
        user_params = FactoryBot.attributes_for(:user, name: 'noname')
        patch :update, params: { id: user.id, user: user_params }
      end

      it '302レスポンスを返す' do
        expect(response).to have_http_status 302
      end

      it 'ルートパスにリダイレクトする' do
        expect(response).to redirect_to root_path
      end
    end

    context 'ログインしていない場合' do
      before do
        patch :update, params: { id: user.id }
      end

      it '302レスポンスを返す' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#destroy' do
    context '管理者権限のあるユーザーの場合' do
      let!(:user) { create(:user) }
      let!(:admin_user) { create(:user, email: 'admin@example.com', admin: true) }
      include SessionsHelper

      before do
        log_in admin_user
      end

      it 'ユーザーを削除できる' do
        expect{ delete :destroy, params: { id: user.id } }.to change{ User.count }.by(-1)
      end

      it 'ユーザー削除後、ユーザー一覧ページにリダイレクトする' do
        delete :destroy, params: { id: user.id }
        expect(response).to redirect_to users_path
      end
    end

    context '管理者権限がないユーザーの場合' do
      let!(:user) { create(:user) }
      include SessionsHelper

      before do
        log_in user
        delete :destroy, params: { id: user.id }
      end

      it '302レスポンスを返す' do
        expect(response).to have_http_status 302
      end

      it 'ルートパスにリダイレクトする' do
        expect(response).to redirect_to root_path
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }

      before do
        delete :destroy, params: { id: user.id }
      end

      it '302レスポンスを返す' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#following' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      include SessionsHelper

      before do
        log_in user
        get :following, params: { id: user.id }
      end

      it 'フォロー一覧を表示する' do
        expect(response).to render_template('show_follow')
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }

      before do
        get :following, params: { id: user.id }
      end

      it '302レスポンスを返す' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#followers' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      include SessionsHelper

      before do
        log_in user
        get :followers, params: { id: user.id }
      end

      it 'フォロー一覧を表示する' do
        expect(response).to render_template('show_follow')
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }

      before do
        get :followers, params: { id: user.id }
      end

      it '302レスポンスを返す' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end
end
