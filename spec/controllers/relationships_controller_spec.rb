require 'rails_helper'

RSpec.describe RelationshipsController, type: :controller do
  describe '#create' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user, email: 'other@example.com') }
      include SessionsHelper

      before do
        log_in user
      end

      it 'フォローすることができる' do
        expect{ post :create, params: { followed_id: other_user.id } }.to change{ user.following.count }.by(1)
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user, email: 'other@example.com') }

      before do
        post :create, params: { followed_id: other_user.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end

  describe '#destroy' do
    context 'ログインしている場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user, email: 'other@example.com') }
      include SessionsHelper

      it 'フォローを削除すること（アンフォロー）ができる' do
        log_in user
        user.follow(other_user)
        relationship = Relationship.last
        expect{ delete :destroy, params: { id: relationship.id } }.to change{ user.following.count }.by(-1)
      end
    end

    context 'ログインしていない場合' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user, email: 'other@example.com') }

      before do
        user.follow(other_user)
        relationship = Relationship.last
        delete :destroy, params: { id: relationship.id }
      end

      it '302レスポンスを返すこと' do
        expect(response).to have_http_status 302
      end

      it 'ログインページにリダイレクトする' do
        expect(response).to redirect_to login_path
      end
    end
  end
end
