require 'rails_helper'

RSpec.describe StaticPagesController, type: :controller do
  describe '#home' do
    context 'ログインしていない場合' do
      before do
        get :home
      end

      it '正常にレスポンスを返すこと' do
        expect(response).to be_success
      end

      it '200レスポンスを返すこと' do
        expect(response).to have_http_status 200
      end
    end
  end
end
