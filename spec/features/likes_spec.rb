require 'rails_helper'

RSpec.feature "Likes", type: :feature, js: true do
  let!(:user) { create(:user) }
  let!(:other_user) { create(:user, email: 'other@example.com') }
  let!(:micropost) { create(:micropost, user: other_user) }

  background do
    visit login_path

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
  end

  feature 'いいね！する' do
    scenario '投稿にいいね！する' do
      visit user_path(other_user)

      expect {
        expect(page).to have_selector "#like-#{micropost.id}", text: 'いいね！ 0 件'
        expect(page).not_to have_css ".like-btn-on"

        click_link "いいね！"

        expect(page).to have_selector "#like-#{micropost.id}", text: 'いいね！ 1 件'
        expect(page).to have_css ".like-btn-on"
      }.to change{ Like.count }.by(1)
    end
  end

  feature 'いいね！を取り消す' do
    scenario '投稿へのいいね！を取り消す' do
      Like.create(user_id: user.id, post_id: micropost.id)

      visit user_path(other_user)

      expect {
        expect(page).to have_selector "#like-#{micropost.id}", text: 'いいね！ 1 件'
        expect(page).to have_css ".like-btn-on"
        
        click_link "いいね！"

        expect(page).to have_selector "#like-#{micropost.id}", text: 'いいね！ 0 件'
        expect(page).not_to have_css ".like-btn-on"
      }.to change{ Like.count }.by(-1)
    end
  end
end
