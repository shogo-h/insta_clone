require 'rails_helper'

RSpec.feature "Users", type: :feature do
  feature 'ユーザー一覧' do
    let!(:users) { create_list(:user, 32) }

    scenario 'ユーザー一覧を表示する' do
      visit login_path

      fill_in 'Email', with: users[0].email
      fill_in 'Password', with: users[0].password
      click_button 'Log in'

      visit users_path

      expect(page).to have_current_path users_path
      expect(page).to have_selector 'h1', text: 'All users'
      expect(all('.users li img').size).to eq(30)
      expect(all('.users li a').size).to eq(30)
      expect(page).to have_selector '.users li a', match: :first, text: users[0].name

      click_link users[0].name, match: :first

      expect(page).to have_current_path user_path(users[0])

      visit users_path

      expect(all('.pagination').size).to eq(4)

      click_link 'Next →', match: :first

      expect(page).to have_current_path '/users?page=2'
      expect(page).to have_selector '.users li a', match: :first, text: users[31].name

      click_link '← Previous', match: :first

      expect(page).to have_current_path '/users?page=1'
    end
  end

  feature 'ユーザー詳細' do
    let!(:user) { create(:user) }
    let!(:microposts) { create_list(:micropost, 31, user: user) }

    scenario 'ユーザー詳細を表示する' do
      visit login_path

      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'

      visit user_path(user)

      expect(page).to have_current_path user_path(user)
      expect(page).to have_selector 'h1', text: user.name
      expect(page).to have_selector 'h1 .gravatar'
      expect(page).to have_selector 'div h3', text: "Microposts (#{user.microposts.count})"
      expect(all('.micropost_box').size).to eq(30)
      expect(all('.micropost_box .gravatar').size).to eq(30)
      expect(all('.micropost_box .user a').size).to eq(30)
      expect(all('.content img').size).to eq(30)
      expect(all('.fa-heart').size).to eq(30)
      expect(all('.fa-comment-dots').size).to eq(30)
      expect(all('.timestamp').size).to eq(30)
      expect(all('.timestamp a').size).to eq(30)
      expect(all('.pagination').size).to eq(2)

      click_link 'Next →'

      expect(page).to have_current_path "/users/#{user.id}?page=2"

      click_link '← Previous'

      expect(page).to have_current_path "/users/#{user.id}?page=1"
    end
  end

  feature 'ユーザー新規作成' do
    scenario 'ユーザーを新規作成する' do
      visit new_user_path

      expect(page).to have_selector 'h1', text: 'Sign up'
      expect {
        fill_in 'Name', match: :first, with: 'Taro Tanaka'
        fill_in 'User Name', with: 'taro'
        fill_in 'Email', with: 'rspec@example.com'
        fill_in 'Password', with: 'password'
        fill_in 'Confirmation', with: 'password'
        click_button 'Create my account'

        user = User.last
        
        expect(page).to have_current_path user_path(user)
        expect(page).to have_selector '.alert-success', text: 'Welcome to the INSTAGRAM CLONE!'
      }.to change{ User.count }.by(1)
    end
  end

  feature 'ユーザー更新' do
    let!(:user) { create(:user) }

    scenario 'ユーザーを更新する' do
      visit login_path

      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'

      visit edit_user_path(user)

      expect(page).to have_current_path edit_user_path(user)
      expect(page).to have_selector 'h1', text: 'Update your profile'
      expect(page).to have_selector 'p', text: '公開情報'
      expect(page).to have_selector 'p', text: '非公開情報'
      expect(page).to have_field 'Name', with: user.fullname
      expect(page).to have_field 'User Name', with: user.name
      expect(page).to have_field 'Email', with: user.email
      expect(page).to have_link 'change', href: 'http://gravatar.com/emails'

      fill_in 'Name', match: :first, with: 'Suzuki Ichiro'
      fill_in 'User Name', with: 'Ichiro'
      fill_in 'Website', with: 'https://www.example.com'
      fill_in 'Introduction', with: 'test'
      fill_in 'Email', with: 'change@example.com'
      fill_in 'Telephone', with: '08011111111'
      find("input[value=1]").set(true)
      click_button 'Save changes'

      expect(page).to have_current_path user_path(user)
      expect(page).to have_selector '.alert-success', text: 'Profile updated'
      expect(page).to have_selector 'h1', text: 'Ichiro'
      expect(page).to have_selector '.introduction', text: 'https://www.example.com'
      expect(page).to have_selector '.introduction', text: 'test'

      visit edit_user_path(user)

      expect(page).to have_field 'Name', with: 'Suzuki Ichiro'
      expect(page).to have_field 'User Name', with: 'Ichiro'
      expect(page).to have_field 'Website', with: 'https://www.example.com'
      expect(page).to have_field 'Introduction', with: 'test'
      expect(page).to have_field 'Email', with: 'change@example.com'
      expect(page).to have_field 'Telephone', with: '08011111111'
    end
  end

  feature 'ユーザー削除', js: true do
    let!(:user) { create(:user) }
    let!(:admin_user) { create(:user, admin: true, email: 'admin@example.com') }

    background do
      visit login_path
    end

    scenario 'ユーザーを削除する（退会処理）' do
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'

      visit edit_user_path(user)

      expect {
        click_link 'account delete'
        page.driver.browser.switch_to.alert.accept

        expect(page).to have_current_path root_path
        expect(page).to have_selector '.alert-success', text: 'User deleted'
      }.to change{ User.count }.by(-1)
    end

    scenario '管理者権限でユーザーを削除する' do
      fill_in 'Email', with: admin_user.email
      fill_in 'Password', with: admin_user.password
      click_button 'Log in'

      visit users_path

      expect {
        within "#user-#{user.id}" do
          click_link 'delete'
        end
        page.driver.browser.switch_to.alert.accept

        expect(page).to have_current_path users_path
        expect(page).to have_selector '.alert-success', text: 'User deleted'
      }.to change{ User.count }.by(-1)
    end
  end
end
