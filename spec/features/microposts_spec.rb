require 'rails_helper'

RSpec.feature "Microposts", type: :feature do
  let!(:user) { create(:user) }
  let!(:micropost) { create(:micropost, user: user) }

  background do
    visit login_path

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
  end

  feature '一覧画面' do
    scenario '表示を確認する' do
      expect(page).to have_current_path root_path
      expect(page).to have_css '.gravatar'
      expect(page).to have_selector '.user', text: user.name

      find("#gravatar-#{user.id}").click

      expect(current_path).to eq user_path(user)

      visit root_path

      within(:css, '.user') do
        click_link user.name
      end

      expect(current_path).to eq user_path(user)

      visit root_path

      expect(page).to have_css "#picture-#{micropost.id}"
    end
  end

  feature '詳細画面' do
    let!(:comment) { create(:comment, micropost: micropost, user: user) }

    scenario 'モーダルを表示する', js: true do
      find("#picture-link-#{micropost.id}").click

      expect(page).to have_css "#user-modal"
      expect(page).to have_selector 'img'
      expect(page).to have_selector 'p', text: micropost.content
      expect(page).to have_selector '.comment_ul'
      expect(page).to have_selector '.comment_li', text: comment.content
      expect(page).to have_css ".comment-box"
      expect(page).to have_css ".comment-btn"
      
      visit root_path

      within(:id, "micropost-#{micropost.id}") do
        find(".fa-comment-dots").click
      end

      expect(page).to have_css "#user-modal"
    end

    scenario 'htmlで表示する' do
      visit micropost_path(micropost)

      expect(current_path).to eq micropost_path(micropost.id)
      expect(page).to have_selector 'img'
      expect(page).to have_selector 'p', text: micropost.content
      expect(page).to have_selector '.comment_ul'
      expect(page).to have_selector '.comment_li', text: comment.content
      expect(page).to have_css ".comment-box"
      expect(page).to have_css ".comment-btn"
    end
  end

  feature 'Micropostの投稿' do
    scenario '投稿する' do
      expect {
        click_link '写真アップロード'

        attach_file 'micropost_picture', "#{Rails.root}/spec/files/test.jpg"
        fill_in 'micropost_content', with: 'テスト'
        click_button 'Post'

        expect(page).to have_current_path root_path
        expect(page).to have_selector '.alert-success', text: "Micropost created!"
      }.to change(user.microposts, :count).by(1)
    end
  end

  feature 'Micropostの削除' do
    scenario '投稿一覧画面で削除する', js: true do
      expect {
        click_link 'delete'
        page.driver.browser.switch_to.alert.accept

        expect(page).to have_current_path root_path
        expect(page).to have_selector '.alert-success', text: "Micropost deleted"
      }.to change(user.microposts, :count).by(-1)
    end
  end
end
