require 'rails_helper'

RSpec.feature "StaticPages", type: :feature do
  feature 'トップページ' do
    scenario '表示の確認' do
      visit root_path

      expect(page).to have_link 'Instagram Clone', href: '/'
      expect(page).to have_link 'Home', href: '/'
      expect(page).to have_link 'Log in', href: '/login'
      expect(page).to have_selector '#q_content_cont'
      expect(page).to have_selector '.search-btn'
      expect(page).to have_selector 'h1', text: 'INSTAGRAM CLONE'
      expect(page).to have_link 'Sign up now!', href: '/users/new'
      expect(page).to have_selector 'small', text: 'Shogo © 2018'
      expect(page).to have_link '利用規約', href: '/terms'
    end
  end

  feature '利用規約ページ' do
    scenario '表示の確認' do
      visit terms_path
      
      expect(page).to have_link 'Instagram Clone', href: '/'
      expect(page).to have_link 'Home', href: '/'
      expect(page).to have_link 'Log in', href: '/login'
      expect(page).to have_selector '#q_content_cont'
      expect(page).to have_selector '.search-btn'
      expect(page).to have_selector 'h1', text: '利用規約'
      expect(page).to have_selector '.center'
      expect(page).to have_selector 'small', text: 'Shogo © 2018'
      expect(page).to have_link '利用規約', href: '/terms'
    end
  end
end
