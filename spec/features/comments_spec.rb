require 'rails_helper'

RSpec.feature "Comments", type: :feature do
  feature 'コメントの投稿' do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user, email: 'other@example.com') }
    let!(:micropost) { create(:micropost, user: other_user) }

    background do
      visit login_path

      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'
    end

    scenario 'コメントを投稿する(モーダル)', js: true do
      visit user_path(other_user)

      find("#picture-link-#{micropost.id}").click

      expect {
        expect(page).to have_css "#user-modal"

        fill_in 'comment_content', with: 'いい写真ですね！'
        find('.comment-btn').click

        expect(page).to have_current_path micropost_path(micropost)
        expect(page).to have_selector ".comment_li", text: 'taro：いい写真ですね！ delete'
      }.to change(micropost.comments, :count).by(1)
    end

    scenario 'コメントを投稿する(html)' do
      visit micropost_path(micropost)

      expect {
        expect(page).to have_current_path micropost_path(micropost)

        fill_in 'comment_content', with: 'いい写真ですね！'
        find('.comment-btn').click

        expect(page).to have_current_path micropost_path(micropost)
        expect(page).to have_selector ".comment_li", text: 'taro：いい写真ですね！ delete'
      }.to change(micropost.comments, :count).by(1)
    end
  end

  feature 'コメントの削除' do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user, email: 'other@example.com') }
    let!(:micropost) { create(:micropost, user: other_user) }
    let!(:comment) { create(:comment, user: user, micropost: micropost) }

    scenario 'コメントを削除する', js: true do
      visit login_path

      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'

      visit user_path(other_user)

      find("#picture-link-#{micropost.id}").click

      expect {
        expect(page).to have_css "#user-modal"
        expect(page).to have_selector ".comment_li", text: 'taro：test delete'
        
        click_link 'delete'
        sleep 5
        page.driver.browser.switch_to.alert.accept

        expect(page).to have_current_path micropost_path(micropost)
        expect(page).not_to have_selector ".comment_li", text: 'taro：test delete'
      }.to change(micropost.comments, :count).by(-1)
    end
  end
end
