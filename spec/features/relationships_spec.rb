require 'rails_helper'

RSpec.feature "Relationships", type: :feature, js: true do
  let!(:user) { create(:user) }
  let!(:other_user) { create(:user, email: 'other@example.com') }

  background do
    visit login_path

    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
  end

  feature 'フォロー' do
    scenario 'other_userをフォローする' do
      visit user_path(other_user)

      expect {
        expect(page).to have_selector "#followers", text: '0'
        expect(page).not_to have_css ".btn-default"

        find("input[value='Follow']").click

        expect(page).to have_selector "#followers", text: '1'
        expect(page).to have_css ".btn-default"
      }.to change{ user.following.count }.by(1)
    end
  end

  feature 'アンフォロー' do
    scenario 'other_userをアンフォローする' do
      Relationship.create(follower_id: user.id, followed_id: other_user.id)

      visit user_path(other_user)

      expect {
        expect(page).to have_selector "#followers", text: '1'
        expect(page).to have_css ".btn-default"
        
        find("input[value='Unfollow']").click

        expect(page).to have_selector "#followers", text: '0'
        expect(page).not_to have_css ".btn-default"
      }.to change{ user.following.count }.by(-1)
    end
  end
end
