require 'rails_helper'

RSpec.feature "Sessions", type: :feature do
  feature 'ログイン/ログアウト' do
    let!(:user) { create(:user) }

    scenario 'ログインして、ログアウトする' do
      visit login_path

      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'

      expect(page).to have_current_path root_path
      
      click_link 'Log out'

      expect(page).to have_current_path root_path
    end
  end
end
