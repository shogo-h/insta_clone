FactoryBot.define do
  factory :comment do
    content { 'test' }
    user
    micropost
  end
end
