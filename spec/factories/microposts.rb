include ActionDispatch::TestProcess

FactoryBot.define do
  factory :micropost do
    content { 'sample' }
    picture { Rack::Test::UploadedFile.new(Rails.root.join('spec/files/test.jpg'), 'image/jpeg') }
    user
  end
end
