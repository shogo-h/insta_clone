FactoryBot.define do
  factory :user do
    name { 'taro' }
    fullname { 'Taro Tanaka' }
    sequence(:email) { |n| "rspec#{n}@example.com" }
    password { 'password' }
  end
end
