require 'rails_helper'

RSpec.describe Relationship, type: :model do
  describe 'Relationship' do
    let!(:user) { create(:user) }
    let!(:other_user) { create(:user, email: 'other@example.com') }

    it 'relationshipが有効であること' do
      relationship = Relationship.new(follower_id: user.id, followed_id: other_user.id)
      expect(relationship).to be_valid
    end

    it 'follower_idが存在しないときrelationshipが無効であること' do
      relationship = Relationship.new(follower_id: nil, followed_id: other_user.id)
      expect(relationship).not_to be_valid
    end

    it 'followed_idが存在しないときrelationshipが無効であること' do
      relationship = Relationship.new(follower_id: user.id, followed_id: nil)
      expect(relationship).not_to be_valid
    end
  end
end
