require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe 'Comment' do
    let!(:user) { build(:user) }
    let!(:micropost) { build(:micropost, user: user) }

    it 'contentがあり、textが140字以内ならば有効であること' do
      comment = Comment.new(content: "test", micropost: micropost, user: user)
      expect(comment).to be_valid
    end

    it 'textが141字以上ならば無効であること' do
      comment = Comment.new(content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", micropost: micropost, user: user)
      expect(comment).not_to be_valid
    end

    it 'contentがなければ無効であること' do
      comment = Comment.new(content: "", micropost: micropost, user: user)
      expect(comment).not_to be_valid
    end
  end
end
