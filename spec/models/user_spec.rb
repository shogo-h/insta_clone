require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'User' do
    it 'name,fullname,email,passwordがあれば、userは有効であること' do
      user = build(:user)
      expect(user).to be_valid
    end

    it 'nameがなければ、userは無効であること' do
      user = build(:user, name: '')
      expect(user).not_to be_valid
    end

    it 'fullnameがなければ、userは無効であること' do
      user = build(:user, fullname: '')
      expect(user).not_to be_valid
    end

    it 'emailがなければ、userは無効であること' do
      user = build(:user, email: '')
      expect(user).not_to be_valid
    end

    it 'passwordがなければ、userは無効であること' do
      user = build(:user, password: '')
      expect(user).not_to be_valid
    end

    it 'emailが重複していれば、userは無効であること' do
      user_a = create(:user, email: 'user@example.com')
      user_b = build(:user, email: 'user@example.com')
      expect(user_b).not_to be_valid
    end

    context 'ユーザーをフォロー/アンフォローする' do
      let!(:user) { create(:user) }
      let!(:other_user) { create(:user, email: 'other@example.com') }

      before do
        user.follow(other_user)
      end

      it 'userがother_userをフォローしていること' do
        expect(user.following?(other_user)).to be_truthy
      end

      it 'userがother_userをアンフォローしていること' do
        user.unfollow(other_user)
        expect(user.following?(other_user)).to be_falsey
      end
    end
  end
end
