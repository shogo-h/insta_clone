require 'rails_helper'

RSpec.describe Micropost, type: :model do
  describe 'Micropost' do
    let!(:user) { create(:user) }
    let!(:micropost) { build(:micropost, user: user) }

    it 'content,pictureがあれば、micropostは有効であること' do
      expect(micropost).to be_valid
    end

    it 'contentがなければ、micropostは無効であること' do
      micropost = build(:micropost, content: '', user: user)
      expect(micropost).not_to be_valid
    end

    it 'pictureがなければ、micropostは無効であること' do
      micropost = build(:micropost, picture: '', user: user)
      expect(micropost).not_to be_valid
    end
    # picture_sizeメソッドのテストは、画像アップロード時に画像を250*250にリサイズしているため、
    # アップロード時のサイズが5MBを超えることがないので、テストは行わない。
  end
end
