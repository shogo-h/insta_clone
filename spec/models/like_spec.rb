require 'rails_helper'

RSpec.describe Like, type: :model do
  describe 'Like' do
    let!(:user) { create(:user) }
    let!(:micropost) { create(:micropost, user: user) }

    it 'user,postがあれば、likeは有効であること' do
      like = Like.create(user_id: user.id, post_id: micropost.id)
      expect(like).to be_valid
    end

    it 'userがなければ、likeは無効であること' do
      like = Like.create(user_id: '', post_id: micropost.id)
      expect(like).not_to be_valid
    end

    it 'postがなければ、likeは無効であること' do
      like = Like.create(user_id: user.id, post_id: '')
      expect(like).not_to be_valid
    end
  end
end
