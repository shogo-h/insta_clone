class AddProfileToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :fullname, :string
    add_column :users, :website, :string
    add_column :users, :introduction, :string
    add_column :users, :number, :string
    add_column :users, :gender, :integer
  end
end
