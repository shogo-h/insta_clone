class AddCotentToMicroposts < ActiveRecord::Migration[5.1]
  def change
    add_column :microposts, :content, :text
  end
end
