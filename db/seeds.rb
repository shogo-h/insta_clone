User.create!(
              fullname:              "Taro Tanaka",
              name:                  "Example User",
              email:                 "admin@example.com",
              password:              "password",
              password_confirmation: "password",
              introduction:          "親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。",
              admin:                 true
            )

50.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(
                fullname:               name,
                name:                   name,
                email:                  email,
                password:               password,
                password_confirmation:  password,
                introduction:           "親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。"
              )
end

#マイクロポスト
users = User.order(:created_at).take(3)
31.times do
  content = "つれづれなるまゝに、日暮らし、硯にむかひて、心にうつりゆくよしなし事を、そこはかとなく書きつくれば、あやしうこそものぐるほしけれ。"
  picture = File.open('app/assets/images/sample.jpg')
  users.each { |user| user.microposts.create!(content: content, picture: picture) }
end

#リレーションシップ
users = User.all
user = users.first
following = users[2..30]
followers = users[2..30]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
