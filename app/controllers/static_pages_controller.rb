class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @feed_items = current_user.feed.includes(:user).paginate(page: params[:page])
      @comment = Comment.new
    end
  end

  def terms
  end
end
