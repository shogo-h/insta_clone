class CommentsController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user, only: :destroy

  def create
    @comment = current_user.comments.build(comment_params)
    if @comment.save
      redirect_to micropost_path(@comment.micropost_id) || root_path
    end
  end

  def destroy
    @comment.destroy
    redirect_to micropost_path(@comment.micropost_id) || root_path
  end

  private
    def comment_params
      params.require(:comment).permit(:content, :micropost_id)
    end

    def correct_user
      @comment = current_user.comments.find_by(id: params[:id])
      redirect_to root_path if @comment.nil?
    end
end
