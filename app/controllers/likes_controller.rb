class LikesController < ApplicationController
  before_action :logged_in_user
  before_action :set_variables

  def create
    Like.create(user_id: current_user.id, post_id: params[:id])
  end

  def destroy
    like = Like.find_by(user_id: current_user.id, post_id: params[:id])
    like.destroy
  end

  private
    def set_variables
      @micropost = Micropost.find(params[:id])
      @id_name = "#like-#{@micropost.id}"
      @id_heart = "#heart-#{@micropost.id}"
    end
end
