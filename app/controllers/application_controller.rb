class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  before_action :search_post

  private

    #ユーザーのログインを確認する
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    def search_post
      @search = Micropost.ransack(params[:q])
      @results = @search.result(distinct: true).includes(:user).paginate(page: params[:page])
    end

end
