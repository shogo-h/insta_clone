class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :new, :create, :destroy]
  before_action :correct_user, only: :destroy

  def index
    @microposts = Micropost.includes(:user).all
  end

  def show
    @micropost = Micropost.find_by(id: params[:id])
    @comments = Comment.where(micropost_id: params[:id])
    @comment = Comment.new
  end

  def new
    @micropost = current_user.microposts.build if logged_in?
  end

  def create
    @micropost = current_user.microposts.build(micropost_params)

    if @micropost.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'new'
    end
  end

  def destroy
    @micropost.destroy
    flash[:success] = "Micropost deleted"
    redirect_to request.referrer || root_url
  end

  private

    def micropost_params
      #画像のみ投稿の場合 params.fetch(:micropost, {}).permit(:picture)
      params.require(:micropost).permit(:content, :picture)
    end

    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
end
